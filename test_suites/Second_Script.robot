*** Settings ***
Library    BuiltIn    

*** Test Cases ***
Test Case1
    [Tags]    Smoke    Integration
    Log To Console    Hello World    #

    ${sq_of_num}=    Test Case1    8
    Log To Console    Square of Number returned is ${sq_of_num}    
    
Test Case2
        Log To Console     Adding Numbers
        Test Case2    3    4  
Conditional Tests
    
    ${val}=    Set Variable     6
    
    Run Keyword If    ${val}==6    Add Numbers    6    7    #For if else      
    ...    ELSE    Log To Console    Cant execute method
        

Loop Tests
    [Tags]    Smoke
    :FOR    ${val}    IN RANGE    1    10
    \    Log To Console    ABC
    \    Log To Console    Value is ${val}        

Asserting Tests
    
    ${val}=    Set Variable    9 
    
    Should Be Equal    ${val}    8    Something went wrong      #for asserting    
*** Keywords ***
Test Case1     #Functions
    [Arguments]    ${number}   #Parameters of Functions 
    ${square_of_num}=    Evaluate    ${number}**2    #Evaluates the value
    Log To Console    ${square_of_num} 
    
Test Case2
    [Arguments]    ${num1}    ${num2}    #For multiple arguments
    
    ${total}=    Evaluate    ${num1}+${num2}  
    Log To Console    ${total}   

Conditional Tests
    [Arguments]    ${num1}    ${num2}    #For multiple arguments
    
    ${total}=    Evaluate    ${num1}+${num2}  
    Log To Console    ${total}  

    
        

        