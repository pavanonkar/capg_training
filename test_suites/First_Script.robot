*** Settings ***
Library    BuiltIn    
Suite Setup    Log To Console    Running TestSuite
Test Setup    Log To Console    Running TestCase

Test TearDown    Log To Console    Completed TestCase
Suite TearDown    Log To Console    Completed TestSuite    
*** Test Cases ***
     
Square Of Number
    [Tags]    Smoke
    Log To Console    Hello World    #
    
    ${sq_of_num}=    Square Of Number    8
    Log To Console    Square of Number returned is ${sq_of_num}  
    Read Python Script  
    
Add Numbers
        Log To Console     Adding Numbers
        Add Numbers    3    4  
Conditional Tests
    
    ${val}=    Set Variable     6
    
    Run Keyword If    ${val}==6    Add Numbers    6    7    #For if else      
    ...    ELSE    Log To Console    Cant execute method
        

Loop Tests
    :FOR    ${val}    IN RANGE    1    10
    \    Log To Console    ABC
    \    Log To Console    Value is ${val}        

Asserting Tests
    
    ${val}=    Set Variable    9 
    
    Should Be Equal    ${val}    8    Something went wrong      #for asserting  
    
 
    
    
      
*** Keywords ***
Square Of Number     #Functions
    [Arguments]    ${number}   #Parameters of Functions 
    ${square_of_num}=    Evaluate    ${number}**2    #Evaluates the value
    Log To Console    ${square_of_num} 
    
Add Numbers
    [Arguments]    ${num1}    ${num2}    #For multiple arguments
    
    ${total}=    Evaluate    ${num1}+${num2}  
    Log To Console    ${total}   

Conditional Tests
    [Arguments]    ${num1}    ${num2}    #For multiple arguments
    
    ${total}=    Evaluate    ${num1}+${num2}  
    Log To Console    ${total}  

Read Python Script
    
    Import Library    ${CURDIR}/Python_utils.py
    
    ${status}=    Number Is Even Odd    8
    
    Log To Console    ${status} 
    

    
   
        

        